eval {
  let settings = {
    version: "0.0.35"
  };

  function _shuffleArray(array) {
    // Courtesy of http://stackoverflow.com/a/12646864/5615927
    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }

  // this will be a lot easier with multi-pass interpretation
  // since these mirrors could have their own choices embedded
  var MIRRORS = [
    "a mirror we've moved on from",
    "a mirror under creative commons",
    "a mirror which told us its name, but we already forgot",
    "a mirror with a new haircut",
    "a mirror made of words",
    "a mirror that reflects later and before",
    "a mirror from new york",
    "a mirror covered over with masking tape, though we can see something move behind it",
    "a mirror from ohio",
    "a mirror with compassion and a mirror with dismay",
    "a mirror miles behind",
    "a lo-fi mirror, a hi-fi one",
    "a mirror filled with thunder; a mirror filled with ice",
    "a happy mirror, a sad mirror, a confused mirror, a straight mirror",
    "a dirty mirror, a clean one",
    "a mirror tinted the slightest bit purple, shattered and at our feet",
    "a mirror carved from wood",
    "a remembered mirror, a forgotten one",
    "a mirror whose edge we are stuck inside",
    "a mirror which reads poems to us",
    "a mirror which is constantly shattering and putting itself back together again",
    "a mirror that convinces everyone they're a little bit gay",
    "a revolutionary mirror, untelevized",
    "a drooping mirror and a growing mirror",
    "a mirror repeating its mistakes, a mirror learning from them",
    "a mirror that asks questions and a mirror that won't",
    "a mirror in a thrift store",
    "a mirror brewing dandelion tea",
    "a mirror that sort of pulses in a way that's nice to look at",
    "a mirror, held in our hand, missing",
    "a mirror we can fit our head through",
    "a mirror who is late for work",
    "a mirror that makes every person into two",
    "a rhyming mirror, one spoken in a language we don't know but with a really nice voice",
    "a mirror from seoul",
    "a mirror which has no name",
    "a used mirror, a new one",
    "a mirror we can eat when we're done with it",
    "a mirror spun from wool",
    "a mirror lit up with sparks",
    "a mirror, viewed from its side",
    "a mirror knit from yarn on the train",
    "a mirror falling from a plane",
    "an hourlong mirror, and an abridged one, a mirror from last week",
    "a mirror which reflects, not left and right, but up and down",
    "a teary-eyed mirror frayed at the seams",
    "a mirror red with rage",
    "a mirror who got laid off last week",
    "a mirror which wants us to improve ourselves",
    "a quiet mirror, a loud mirror, a verybigandsmall mirror",
    "a mirror whose surface sort of wobbles when we get upset",
    "a mirror made of bees",
    "a mirror in the pictureframe we're trying to look past",
    "a mirror filled with coal",
    "a mirror planted in soil and freshly watered",
    "a mirror covered in signatures",
    "a mirror in which everyone is half korean",
    "a mirror where every face becomes our own, and our own becomes every other",
    "a mirror connecting cities",
    "a mirror frustrated and tired",
    "a mirror made of paints",
    "a mirror made of movements",
    "a mirror made of sounds",
  ];

  _shuffleArray(MIRRORS);

  function _createMirrors(count) {
    let mirrors = [];
    for (i = 0; i < count; i++) {
      mirrors.push(MIRRORS.pop());
    }
    return mirrors.join('; ');
  }

  function firstMirrors(match, string, index) {
    return _createMirrors(bml.randomInt(2, 3));
  }

  function secondMirrors(match, string, index) {
    return _createMirrors(bml.randomInt(3, 5));
  }
  
  provide({
    settings,
    firstMirrors,
    secondMirrors
  });
}

<div class="poem">
<p>
it's {(been), ()} {(hard), (harder)} these days to {(think), (care)} about {(poems), (words), (lines)}---to {(think), (care)} about {(tree), (cliff), (water), (bone), (apple), (wood)}songs or {(purple paints), (little lakes), (little words), (little worlds), (painted walls), (burning clouds)}---to {(sit), (be), (pause)} with {(beauty), (quiet)} and share a {(breath), (sound), (look), (glance)}, to {(half), ()}close our eyes and smile.
</p>
<p>
helga davis said, "it's a very important thing right now that we have other kinds of mirrors to hold up to the world, and to ourselves"
</p>
<p>
{call firstMirrors}
</p>
<p>
we {(ask), (wonder)} {(whether), (if), (when)} we'll {(ever), ()} be able to {(visit), (see), (feel)} the {(skypoems), (kitestrings), (lights)} again, {(whether), (if)} it {(has), (needs)} to be about {(carbon), (justice), (capitalism), (struggle), (the future), (landlords), (banks), (copyright)}.
</p>
<p>
{call secondMirrors}
</p>
<p>
we find a few {(and), (but)} imagine {(some), () 80} more---we {(don't really expect), (aren't sure), (don't think)} these will be enough, {(but), (and)} maybe that's okay. maybe the {(poems), (skypoems), (kitestrings), (lights), (colorchords), (orchestra) 2, (opera) 2, (operahouse) 1} can wait.
</p>
<p>
a mirror where everything is reflected, exactly as it is
</p>
</div>

this dynamic poem was on display at [cj one gallery](https://www.cjonegallery.com/)'s union square location march 5th to 14th as part of a group show with [digiana](https://www.instagram.com/nydigianagroup/) organized by [seungjin lee](https://www.instagram.com/seungjin888). it's installed with what i'm considering the "v2.0" of the poetron{( ("v1.0" of which was initially shown at [a group show put on by ray lc](https://www.eventbrite.com/e/technology-and-social-good-tickets-64092204509) in july 2019)), ()}. visitors are invited to press a big glowing button, causing a new {(version), (realization)} of the poem to be {(created), (generated)} and printed on a receipt for them to take home. 

the poem is a reflection of sorts on a quote from helga davis on her podcast, [in conversation with jacqueline woodson](https://www.newsounds.org/shows/helga/1) around 43:40.

<figure>
  <img src="/img/posts/other-kinds-of-mirrors/cj_one_installation_1.jpg" alt="installation at CJ One Gallery">
  <figcaption>installed at <a href="https://www.cjonegallery.com/">cj one gallery</a>, union square</figcaption>
</figure>

<figure>
  <img src="/img/posts/other-kinds-of-mirrors/cj_one_installation_2.jpg" alt="installation at CJ One Gallery">
  <figcaption>installed at <a href="https://www.cjonegallery.com/">cj one gallery</a>, union square; "miku scans" by <a href="http://www.romankalinovski.com/">roman kalinovski</a> on the left</figcaption>
</figure>

the giant pile of paper around the box as installed is a printout of a random (markov) walk through the poem's "source code," which is freely available [here](https://gitlab.com/ajyoon/poetron/-/tree/master/poems/other_kinds_of_mirrors). the poem is written in my dynamic poetry programming language, [bml](https://bml-lang.org/), which allows writers to concisely express chance operations in text, for instance:

<code>
[[it's {(been), ()} {(hard), (harder)} these days to {(think), (care)} about {(poems), (words), (lines)}---to {(think), (care)} about {(tree), (cliff), (water), (bone), (apple), (wood)}songs or {(purple paints), (little lakes), (little words), (little worlds), (painted walls), (burning clouds)}]]
</code>

<figure>
  <img src="/img/posts/other-kinds-of-mirrors/workshop_testing.jpg" alt="installation from the workshop">
  <figcaption>testing in my workshop</figcaption>
</figure>

the box itself was my first baby step into woodworking; i bought a little wooden storage chest, took off the top, flipped it upside-down, and cut {(some), (a few)} holes on it using holesaw. to make the physical experience of touching the box {(feel better), (nicer), (more nice)}, I sanded down all its edges and corners. inside, it's a raspberry pi hooked up to a little arcade input interface and a receipt printer (equipped with a cutter---i learn from my mistakes!)

to date, i haven't been able to establish an ssh tunnel between the rasperry pi and my personal linux box, so i had to cave and use my work mac, which made me a little {(), (bit)} sad. one fun bit with the pi {(programming), (setup)} was [hooking it up with systemd](https://gitlab.com/ajyoon/poetron/-/blob/6fdd61c33ffa5eb8043a264228a7f0258d35935a/poems/other_kinds_of_mirrors/scripts/mirrors.service) so that it automatically starts on boot and recovers from crashes. i also [built in a graceful shutdown mechanism](https://gitlab.com/ajyoon/poetron/-/blob/6fdd61c33ffa5eb8043a264228a7f0258d35935a/poetron/button.py#L26) so it can be turned off without risking disk corruption.

by the way, the poem above (and this entire {(post), (blog post), (writeup)} too) is live bml---refresh to see it change.
