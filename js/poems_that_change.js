// Coin & Dice Visualizations
var coin_and_dice_waypoint = new Waypoint({
  element: document.getElementById("coin-and-dice-viz-wrapper"),
  offset: '100%',
  handler: function() {
    var coin_data = [
      {"result": "heads", "probability": 0.5},
      {"result": "tails", "probability": 0.5}
    ];
    var coin_viz = d3plus.viz()
          .container("#coin-viz")
          .title("coin toss")
          .width(300)
          .height(250)
          .data(coin_data)
          .type("bar")
          .id("result")
          .x("result")
          .y("probability")
          .timing({"transitions": 500})
          .draw();

  var dice_data = [
    {"result": 1, probability: 0.167},
    {"result": 2, probability: 0.167},
    {"result": 3, probability: 0.167},
    {"result": 4, probability: 0.167},
    {"result": 5, probability: 0.167},
    {"result": 6, probability: 0.167},
  ];
  var dice_viz = d3plus.viz()
        .container("#dice-viz")
        .title("dice roll")
        .width(300)
        .height(250)
        .data(dice_data)
        .type("bar")
        .id("result")
        .x("result")
        .y("probability")
        .timing({"transitionsl": 500})
        .draw();

    // Trigger render only once
    this.destroy();
  }
});

// Marbles Visualization
var marbles_waypoint = new Waypoint({
  element: document.getElementById("marbles-viz-wrapper"),
  offset: '100%',
  handler: function() {
    var marbles_data = [
      {"color": "red", probability: 0.45},
      {"color": "blue", probability: 0.36},
      {"color": "yellow", probability: 0.18}
    ];
    var marbles_attrs = [
      {"color": "red",    "hex": "#991111"},
      {"color": "blue",   "hex": "#111199"},
      {"color": "yellow", "hex": "#ffff66"}
    ];
    var marbles_viz = d3plus.viz()
          .container("#marbles-viz")
          .title("marbles in a bag")
          .width(300)
          .height(250)
          .data(marbles_data)
          .type("bar")
          .id("color")
          .x("color")
          .y("probability")
          .attrs(marbles_attrs)
          .color("hex")
          .legend(false)
          .timing({"transitions": 500})
          .draw();

    // Trigger render only once
    this.destroy()
  }
});

// Word bag visualization
var word_bag_waypoint = new Waypoint({
  element: document.getElementById("words-viz-wrapper"),
  offset: '100%',
  handler: function() {
    var words_data = [
      {"word": "i", "number in bag": 3},
      {"word": "have", "number in bag": 7},
      {"word": "nothing", "number in bag": 6},
      {"word": "to", "number in bag": 2},
      {"word": "say", "number in bag": 5},
      {"word": "and", "number in bag": 10},
      {"word": "am", "number in bag": 6},
      {"word": "saying", "number in bag": 6},
      {"word": "it", "number in bag": 1},
      {"word": "is", "number in bag": 6},
      {"word": "poetry", "number in bag": 12},
    ];

    var words_viz = d3plus.viz()
          .container("#words-viz")
          .title("A bag of words")
          .width(350)
          .height(350)
          .data(words_data)
          .type("bar")
          .id("word")
          .x("word")
          .y("number in bag")
          .timing({"transitions": 500})
          .draw();

    // Trigger render only once
    this.destroy()
  }
});

// Markov chain as bar graphs visualizations
var markov_bar_graph_waypoint = new Waypoint({
  element: document.getElementById("markov-viz-wrapper"),
  offset: '100%',
  handler: function() {
    var mk_1_data = [
      {"next word": "poetry", "probability": 1}
    ];
    var mk_1_viz = d3plus.viz()
          .container("#mk-1-viz")
          .title("Words following \"that\"")
          .width(250)
          .height(250)
          .data(mk_1_data)
          .type("bar")
          .id("next word")
          .x("next word")
          .y("probability")
          .timing({"transitions": 500})
          .draw();

    var mk_2_data = [
      {"next word": "that", "probability": 0.4},
      {"next word": "poetry", "probability": 0.6}
    ];
    var mk_2_viz = d3plus.viz()
          .container("#mk-2-viz")
          .title("Words following \"is\"")
          .width(250)
          .height(250)
          .data(mk_2_data)
          .type("bar")
          .id("next word")
          .x("next word")
          .y("probability")
          .timing({"transitions": 500})
          .draw();

    var mk_3_data = [
      {"next word": "is", "probability": 0.5},
      {"next word": "that", "probability": 0.5}
    ];
    var mk_3_viz = d3plus.viz()
          .container("#mk-3-viz")
          .title("Words following \"poetry\"")
          .width(250)
          .height(250)
          .data(mk_3_data)
          .type("bar")
          .id("next word")
          .x("next word")
          .y("probability")
          .timing({"transitions": 500})
          .draw();

    // Trigger render only once
    this.destroy();
  }
});
