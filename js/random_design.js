function shuffleArray(array) {
  // Courtesy of http://stackoverflow.com/a/12646864/5615927
  for (var i = array.length - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
}

function processOneRandomOrderList(listElement) {
  var contributors = listElement.children;
  var shuffledContributors = shuffleArray(Array.prototype.slice.call(contributors));
  var newContributorElements = document.createDocumentFragment();
  for (var i = 0; i < shuffledContributors.length; i++) {
    newContributorElements.appendChild(shuffledContributors[i].cloneNode(true));
  }
  listElement.innerHTML = null;
  listElement.appendChild(newContributorElements);

  // If a contributor anchor is in the URL, the shuffle would have likely moved the
  // targeted div, so we need to reset the location hash to tell the browser to
  // refocus after the switcharoo
  if(window.location.hash) {
    var previousHash = window.location.hash;
    window.location.hash = "";
    window.location.hash = previousHash;
  }
}

function processRandomOrderLists() {
  var lists = document.getElementsByClassName('random-order-list');
  for (var i = 0; i < lists.length; i++) {
    processOneRandomOrderList(lists[i]);
  }
}

document.onreadystatechange = function () {
  document.addEventListener("DOMContentLoaded", function(event) {
    processRandomOrderLists();
  });
};
