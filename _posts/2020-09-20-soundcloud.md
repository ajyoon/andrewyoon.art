---
layout: post
title: I Dropped My SoundCloud Subscription and You Can Too
description: If a tree falls in a forest...
date: 2020-09-20
public: true
permalink: /blog/soundcloud
category: thoughts
image: /img/posts/soundcloud/removed.jpg
---
Today my SoundCloud Pro subscription lapsed. It's been a long time coming, and since many of my friends remain subscribed I thought I'd share some thoughts on it.

### If a tree falls in a forest...

In theory, SoundCloud's most important offering to musicians is an audience. It wants to be a social network for music, connecting listeners to artists through recommendations and sharing, allowing listeners to engage through likes, comments, reposts, and private messaging. To be sure, this has worked out for many artists, particularly with hip hop figures like [Chance the Rapper](https://www.pastemagazine.com/music/chance-the-rapper/soundcloud-is-not-going-out-of-business-thanks-to/) gaining early attention through the platform.

But outside a small handful of scenes this platform engagement is practically nonexistent. Take the classical composition scene---Du Yun, a recent Pulitzer winner who's [regularly](https://www.nytimes.com/2019/08/30/arts/music/du-yun-composer.html) [in](https://www.latimes.com/entertainment-arts/story/2020-05-01/coronavirus-what-to-watch-angels-bone-du-yun-la-opera) [mainstream](https://www.npr.org/sections/deceptivecadence/2017/04/10/523304153/du-yun-wins-music-pulitzer-for-angels-bone) [news](https://www.newyorker.com/magazine/2018/11/19/du-yuns-electronic-evolution-at-miller-theatre) as a composer rock star, has [444 followers](https://soundcloud.com/duyun). With more popular appeal, Caroline Shaw (also a Pulitzer winner) is doing a little better at [1567](https://soundcloud.com/carolineshaw). The most followers I could find for anyone typically associated with the "new music" scene is Brian Eno, at a whopping [7888 followers](https://soundcloud.com/brian-eno-official), a number dwarfed by countless mildly attractive [VSCO girls](https://en.wikipedia.org/wiki/VSCO_girl) on instagram.

Does this dearth of engagement mean these musicians just aren't that popular? It's true that the "new music" scene's audience is famously small, but consider artists like Brian Eno, whose most-listened-to track on SoundCloud has [6875 plays](https://soundcloud.com/brian-eno-official/an-ending-ascent-remastered) while the same on Spotify has [82 million](https://open.spotify.com/track/1vgSaC0BPlL6LEm4Xsx59J?si=cROIXJH6Tqak8FS9GNIPAQ). Or consider Philip Glass, at [7894 plays](https://soundcloud.com/philip-glass-official/metamorphosis) versus [55 million](https://open.spotify.com/track/1QmhkjuUlosROqKk59sBSK?si=YbnXQgN5Sh2oArmY-FEIIA).

Well, as a certified Little Guy, most of the engagement I get through the platform is spam.

<figure class="img-wrapper">
  <img class="medium-height-img" src="/img/posts/soundcloud/spam-likes.jpg" alt="Screenshot of soundcloud notifications of accounts 'liking' tracks whose profile pictures say 'Buy real soundcloud followers'">
  <caption>Two years later both of these fake accounts still exist</caption>
</figure>

This is no innocent Nigerian Prince---these are ads openly peddling fake traffic and engagement grifts. Beyond direct messaging, liking, and sharing from these fake accounts, I've seen huge play surges on tracks, only to see in the "your top listeners" insights (which creepily exist, by the way) that it was a demonstration of the bots' traffic generation abilities and a request for you to pay for more. These operations prey on the frustrations and hopes of little guys trying to cultivate a following, and SoundCloud has continually failed do anything about it.

To be sure, I have benefited from some marginal growth and listener upkeep on the platform, but it doesn't hold a candle to what I get from organizing and playing shows. So essentially I was just using it for audio hosting, and that is _certainly_ not worth $12-16 a month.

### Breaking Up

SoundCloud's free tier allows users to upload up to 3 hours of audio, which is plenty for many musicians who work in typically short forms like songwriting. For those working in longer forms, like ambient or improvisatory ones, this limit can be reached very quickly. Thanks to my habit of dumping long improv tracks there, I easily had almost 12 hours on it. Now that my subscription has expired, almost all of that music has been archived unless I pay up again. This is no mere delisting---the tracks are actually taken down:

<figure class="img-wrapper">
  <a href="https://soundcloud.com/andrewjyoon/othergreen">
    <img class="medium-height-img" src="/img/posts/soundcloud/removed.jpg" alt="SoundCloud error message saying 'This track was not found. Maybe it has been removed'">
  </a>
</figure>

I'm still able to download the files myself to recover them, but any public links to or embeddings of them are broken. I also have no say in which tracks are and aren't archived; the most recently posted 3 hours survive, and unless I pay up the only way to tweak this is to permanently delete all but my chosen tracks.

### Alternatives

There are many alternatives to SoundCloud, none of which exactly capture its social network aspirations. [Bandcamp](https://bandcamp.com/), operating on a marketplace model, offers essentially unlimited free embeddable hosting and [relatively equitable price cuts](/blog/bandcamp). [MediaGlobin](https://mediagoblin.org/) can function as a sort of decentralized SoundCloud, but [isn't used much](https://wiki.mediagoblin.org/Live_instances) and is a bit of a lift to self-host. YouTube of course offers unlimited free hosting, but is somewhat awkward for musicians since it's video-only. With a little command-line foo it's easy to make audio-only videos:

```sh
# Bash function to convert a still image and audio file to video
# Requires https://ffmpeg.org
#
# Arg 1: Path to an image (1920x1280 recommended)
# Arg 2: Path to audio file
# Arg 3: Path to output video file
function audio2video {
    ffmpeg -r 1 -loop 1 -i $1 -i $2 -acodec copy -r 1 -shortest $3
}
```

The more DIY-inclined might turn to file hosting services, [of](https://www.digitalocean.com/products/spaces/) [which](https://docs.netlify.com/large-media/overview/) [there](https://azure.microsoft.com/en-us/services/storage/) [are](https://aws.amazon.com/s3/) [many](https://cloud.google.com/storage/). I ended up going with [B2](https://www.backblaze.com/cloud-storage), the hosting service provided by the backup storage company [Backblaze](https://www.backblaze.com/). For storage it charges $0.005 per GB per month, and for downloads $0.01 per GB---essentially free for my purposes. It has a nice command line tool for uploads, but also a web interface for uploading and managing files. These days browsers don't even need special embedded media players---the default [HTML5 audio widget](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/audio) works just fine.

So I went through my [extremely stale work list](/work), replicated every soundcloud link into a B2 upload, and swapped out the SoundCloud links with direct file links. I plan to keep duplicating most things onto my YouTube channel, as this remains a very common way people look for music.

There are, of course, a few shortcomings of this approach. 
- You need to have a website. A basic point, but not to be taken for granted among lots of musicians who'd rather practice than wrangle CSS.
- There's no automatic way for "followers" to get notified about new tracks. Instead you need to manually announce them through other channels like a [mailing list](https://tinyletter.com/andrewyoon) or other social media. You could do it with RSS, but let's be real, nobody uses RSS anymore :(
- You have to roll your own presentation format for tracks. You can do some weird hacks to avoid this like using wordpress blog entries as track entries, but regardless it takes some investment.

Going forward I want to make the presentation format for the tracks nicer by giving them dedicated pages with longer, richer descriptions and [sexier playback widgets](https://www.npmjs.com/search?q=audio%20player). This would hopefully give me better insight into play stats and make it feel a little more robust than a list of MP3 downloads.

Looking back, I think what kept me on SoundCloud for so long was what it _wants_ to be---a music-oriented social network which fosters discovery and human relationships. I still believe there remains a place for such a network, but SoundCloud isn't it. I won't pretend to know all the details of _why_ SoundCloud failed, but I can only hope what comes next will be decentralized in the manner of [Pixelfed](https://pixelfed.social/) and [PeerTube](https://joinpeertube.org/) so its community can reject practices which fail artists and listeners alike.
