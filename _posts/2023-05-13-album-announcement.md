---
layout: post
title: The Complete Bach Cello Suites on Melodica comes out June 3!
date: 2023-05-13
public: true
permalink: /blog/cello-suite-album-announcement
category: thoughts
---

Dear friends,

[The Complete Bach Cello Suites on Melodica](https://yoonandrew.bandcamp.com/album/the-complete-bach-cello-suites-on-melodica) comes out June 3! My hobby has officially gotten out hand. Over the last couple years I've been learning to push this funny instrument far beyond anything I imagined it could do, to the point where I daresay it sounds...good? It's been an incredible joy learning these timeless classics in a context that insists on playful invention, and I'm so so excited to share it with you!

<figure class="img-wrapper">
  <a href="https://yoonandrew.bandcamp.com/album/the-complete-bach-cello-suites-on-melodica">
    <img src="/img/posts/album-announcement/cover_small.webp"
        alt="An album cover with me playing melodica and the text 'The Complete Bach Cello Suites on Melodica / Andrew Yoon'"/>
  </a>
</figure>

You can [buy the pre-release on Bandcamp **today**](https://yoonandrew.bandcamp.com/album/the-complete-bach-cello-suites-on-melodica) and listen to some select movements. You can also watch me play [the entire first suite on Youtube here](https://www.youtube.com/watch?v=Ga9REaxZR5o)! We recorded video to go along with the entire album; I'll be releasing the rest over the next few weeks.

And to help kick off the release, on June 3 the [Melodica Drone & Bach Quartet](https://mdbq.org) will be playing an absolutely STACKED concert at the [Stone Circle Threatre](https://stonecircletheatre.org/) in Ridgewood. We'll be playing the Bach double concerto, a monstrous organ piece, a new piece by [Hannah Cai Sobel](https://hannahcaisobel.squarespace.com/), and so much more. [Early tickets are available now at a discount](https://theticketing.co/e/june03)!

<figure class="img-wrapper">
  <a href="https://theticketing.co/e/june03">
    <img src="/img/posts/album-announcement/6_3_poster_small.webp"
        alt="The melodica quartet looking happy after a concert"/>
  </a>
</figure>

Lastly, in addition to the quartet's upcoming concert on June 3, we'll be holding our rescheduled open jam in Central Park on June 17 at 2PM, [which you can RSVP for here](https://docs.google.com/forms/d/e/1FAIpQLSeyjxGVrLOVyTmdQvExiLMWgDd2Rd1ldnRETZyUlEcelKhd0A/viewform)!

Alright, that's about words enough. Love ya!  
~ay
