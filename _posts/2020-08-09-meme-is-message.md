---
layout: post
title: The Meme is the Message
description: Critical Shitposting Theory
date: 2020-08-09
public: true
permalink: /blog/the-meme-is-the-message
category: thoughts
---
<style>
.meme-wrapper {
    text-align: center;
}
</style>
<script>
function toggleMedia(elementId) {
    function handler(event) {
        var memeElement = document.getElementById(elementId);
        var buttonElement = event.target;
        if (memeElement.style.display === 'none') {
            memeElement.style.display = 'block';
            buttonElement.innerHTML = 'Hide gross meme';
        } else {
            memeElement.style.display = 'none';
            buttonElement.innerHTML = 'Show gross meme again';
        }
    }
    return handler;
}
</script>
_cw: raunchy and bigoted memes embedded below. while this blog is under a [CC0 license](https://creativecommons.org/publicdomain/zero/1.0/), the memes within are not; I've linked each to sources_

On February 5th, 2020, a meme was born:

<figure class="meme-wrapper">
  <a href="https://www.facebook.com/105184580854558/photos/a.105214447518238/188770049162677">
    <img class="medium-height-img" src="/img/posts/meme-message/origin.jpg" alt="original">
  </a>
</figure>

Behold!---[the comparison doge](https://knowyourmeme.com/memes/swole-doge-vs-cheems)! In this format, a very strong and powerful doge from some point in the past compares to an inadequate contemporary one. The meme flew under the radar for months, until in mid-May variations of it began circulating around twitter and reddit and its popularity exploded.

<figure class="meme-wrapper">
  <a href="https://www.reddit.com/r/dankmemes/comments/gptknw/it_do_be_like_that/">
    <img class="medium-height-img" src="/img/posts/meme-message/nokia.jpg" alt="Phones in 90s: (nokia) I'm the strongest material known to man. My battery power is ulminited. Phones in 2020: (apple) pls don't touch me or I'll crack. I haven't been charged for 3 hours, I'll commit die.">
  </a>
</figure>

<figure class="meme-wrapper">
  <a href="https://www.reddit.com/r/ProgrammerHumor/comments/grreuq/i_code_in_html_and_css/">
    <img class="medium-height-img" src="/img/posts/meme-message/programmers.jpg" alt="Programmers in 1960s: with this software we shall fly to the moon and back. Programmers in 2020: Halp me pliz, I can't exit vim.">
  </a>
</figure>

<figure class="meme-wrapper">
  <a href="https://www.reddit.com/r/dankmemes/comments/gsqzpt/dads_eats_anything_mom_makes/">
    <img class="medium-height-img" src="/img/posts/meme-message/leaf-in-soup.jpg" alt="My dad: I devour every part of the chicken. Me: there is a leaf in my soup">
  </a>
</figure>

In addition to featuring the Doge of olde, the meme brings in the relatively new character of Cheems, the modern lackey doge. At risk of outing myself as a, um, occasional, reddit lurker, I saw a _lot_ of these memes, and started noticing a trend.

<figure class="meme-wrapper">
  <a href="https://www.reddit.com/r/dankmemes/comments/i2uo1m/i_cannot_live_without_charli_dmicrophone/">
    <img class="medium-height-img" src="/img/posts/meme-message/tiktok.jpg" alt="Teens in 1914: Yesss I'm 18, can I go to war now? Teens now: (holding a phone with TikTok on it) Orange man banned my favourite app">
  </a>
</figure>
<figure class="meme-wrapper">
  <a href="https://www.reddit.com/r/Animemes/comments/gl1m56/becoming_an_anime_girl_is_my_ultimate_dream/">
    <img class="medium-height-img" src="/img/posts/meme-message/dad.jpg" alt="My dad at 17: Darling I'm home, how are the kids? Me at 17: Damn I wish I were a cute anime girl">
  </a>
</figure>

The fundamental structure of the format is comparing something which was old and good to something today which is weak and inadequate. Without stepping outside the bounds of this structure, it's difficult to make any positive statement about the current state of things. This plays well into the "doomer" aesthetic of disaffected young people, especially where Cheems plays "me" and makes the joke a self-deprecating one, but the format becomes much more pernicious when Cheems does not play "me", but instead some "other". In many cases, the "other" is a [Social Justice Warrior](https://en.wikipedia.org/wiki/Social_justice_warrior) strawman while Doge plays some model minority or imaginary well behaved progressive of the past.

<figure class="meme-wrapper">
  <a href="https://www.reddit.com/r/dankmemes/comments/gr112o/more_like_fakeminists/" >
    <img class="medium-height-img" src="/img/posts/meme-message/feminists-strawman.jpg" alt="Feminists in the 1960s: We are fighting to get the same rights as man. Feminists now: Scary man raped me by asking the time, help!">
  </a>
</figure>

In either of these uses, the format is almost inevitably nostalgic and regressive, even reactionary. To escape this ideological tug, an author has no choice but to go outside the normal bounds of the template.

This observation has gotten me thinking more seriously about the kinds of subtexts that are communicated not by memes, but by the underlying meme _formats_. While many are ideologically neutral, like [comparison formats](https://knowyourmeme.com/memes/drakeposting) or [absurdist ones](https://knowyourmeme.com/memes/galaxy-brain), many others do have ideological slants to them, and these slants often lean right.

Consider the use of webcomics as fill-in-the-blanks meme templates. While, as we've seen, the comic used in the template carries subtext and structures the joke, it's also important to the consider who the comic's author is and what other comics they've made. I'm alluding, of course, to Stonetoss, the [white supremacist everything-phobe](https://rationalwiki.org/wiki/StoneToss) whose comics were used for countless memes in 2017-19. 

<figure class="meme-wrapper">
  <a href="https://knowyourmeme.com/photos/1382374-go-on-then-what-are-they-saying">
    <img class="medium-height-img" src="/img/posts/meme-message/stonetoss-sneaky.jpg" alt="Man 1: Sir, we've decrypted the secret Nazi communications. We can see their chatter. Man 2: Bloody hell! Go on then, what are they saying? Man 1: Pee is stored in balls">
  </a>
</figure>

While some memes using Stonetoss comic templates are harmless enough---funny, even---they usually retain the Stonetoss watermark, funnelling unsuspecting readers toward the plainly fascist comic. This, in turn, increases the chance that some of Stonetoss's more slimy comics are shared and used for memes, like the grossly ableist ["Acquired Tastes"](https://knowyourmeme.com/memes/acquired-tastes) format.

<figure class="meme-wrapper">
  <a href="https://knowyourmeme.com/photos/1460635-acquired-tastes" id="gross-meme-1">
    <img class="medium-height-img" src="/img/posts/meme-message/stonetoss-gross.jpg" alt="Airpod users and headphone users shake hands, and together look in disgust at a caricature disabled person labeled 'people who play music out loud in public'">
  </a>
  <br/>
<button id="hide-gross-meme-button-1">Hide gross meme</button>
</figure>
<script>
document.getElementById('hide-gross-meme-button-1')
    .addEventListener('click', toggleMedia('gross-meme-1'));
</script>

## Subverting the Subversive

These days, Stonetoss memes are rare. Their death was not of natural causes---an organized campaign was run to spread awareness of his ugliness and toxicity, largely through the subreddit [r/antifastonetoss](https://www.reddit.com/r/antifastonetoss/). This resistance took place along three main fronts:

<ol>
  <li>Spreading great memes with his comics, but with his signature replaced with "Stonetoss is a Nazi"
    <figure class="meme-wrapper">
      <a href="https://www.reddit.com/r/antifastonetoss/comments/c4qoci/first_edit_please_be_gentle/">
        <img class="medium-height-img" src="/img/posts/meme-message/stonetoss-edit.jpg" alt="Alexa, what's the maximum diameter a human asshole can stretch? 4.5-5.5 inches. Alex...what's the diameter of an Amazon Echo? (Alexa sweating)">
      </a>
    </figure>
  </li>
  <li>Consistently <a href="https://www.google.com/search?q=site%3Areddit.com+%22stonetoss+is+a+nazi%22">commenting the same</a> on posts made by [un]witting memers using his formats.</li>
  <li>Co-opting the comics to spread positive/lefty messages
    <figure class="meme-wrapper">
      <a href="https://www.reddit.com/r/antifastonetoss/comments/g9pptc/everyones_hiding_something_3/">
        <img class="medium-height-img" src="/img/posts/meme-message/stonetoss-edit-2.jpg" alt="MAGA person to rainbow flag holder: Why have that around? Rainbow flag holder: why not? People should be proud of who they are. Maga person (thought bubble of trans flag): I should? Rainbow flag holder gives sly smile.">
      </a>
    </figure>
  </li>
</ol>

Awareness quickly spread about Stonetoss, and today memes with his comics are rarely found outside alt-right spaces.

In the case of Doge vs Cheems, the subversion of it was not organized---and mostly not even intentional---but used techniques consistent with the Stonetoss resistance. One was subverting the format to argue for the continual strength or weakness of a group of people:

<figure class="meme-wrapper">
  <a href="https://www.reddit.com/r/DankLeft/comments/gzvzar/some_things_dont_change/">
    <img class="medium-height-img" src="/img/posts/meme-message/gays.jpg" alt="Gays 60 BC: (Doge in Roman armor) my bond with my lover will make our shield wall unbreakable. Gays today: (Doge as a modern anarchist with a rainbow flag and a shield spray painted with the Three Arrows symbol) my bond with my lover will make our shield wall unbreakable">
  </a>
</figure>

The other was a sort of muddying of waters by using the format for meta-humor or absurdist purposes:

<figure class="meme-wrapper">
  <a href="https://www.reddit.com/r/dankmemes/comments/gsv9qt/good_boys/">
    <img class="medium-height-img" src="/img/posts/meme-message/hug.jpg" alt="Doge and cheems hugging. Doge: Hey guys, stop looking down on my bro. He's a good boy as well">
  </a>
</figure>

<figure class="meme-wrapper">
  <a href="https://www.reddit.com/r/dankmemes/comments/gug617/am_so_funni/">
    <img class="medium-height-img" src="/img/posts/meme-message/wheels.webp" alt="Wheels on shopping carts be like: (Three doges and one cheems)">
  </a>
</figure>

In the natural lifecycle of a meme, structural subversions like these often signal the format has run its course since the default joke has been exhausted. With Doge vs Cheems, all of these variations came toward the end of its time, and today the format is rarely seen without subversions.

### Toward a Left Meme Practice

It's a common criticism that "the Right can't meme" since they intersect with boomer comics and [impact font](https://knowyourmeme.com/memes/impact) MAGA trash, but this fails to account for the way ideas can be encoded in formats themselves. While leftists spread spicy commentary largely through overt and inflexible twitter screenshots, the Right spreads ideas through teenagers who don't even realize they're doing so. While we have proven models for playing defense against this form of propaganda, I believe there is a need to further organize these efforts to more consistently replicate the Antifa Stonetoss success. In recent weeks, [several](https://www.reddit.com/r/dankmemes/comments/i2fxod/chad_san/) [right-wing](https://www.reddit.com/r/dankmemes/comments/i1odcb/credit_to_uborisbop69_for_the_template/) [meme](https://www.reddit.com/r/MadeMeSmile/comments/i5ug8d/good_cops_are_too_often_overlooked/) [propaganda campaigns](https://www.reddit.com/r/facepalm/comments/i4tler/dumbass_tik_tok_assing_ass/) have flourished amid delayed and disorganized resistance---we can do better.

More importantly, I believe there is a need to start playing offense in this arena by more proactively creating and bolstering formats that guide readers and creators alike to a more coherent and adversarial worldview. In the next installment of this series, we'll dig deeper into what Shitposting Praxis™ looks like by analyzing some lefty formats found in the wild and trying to understand what makes some successful and others not.
