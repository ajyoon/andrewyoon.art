---
layout: post
title: Jam with the Melodica Drone & Bach Quartet Saturday 4/29
date: 2023-04-20
public: true
permalink: /blog/mdbq-april-2023-jam
category: thoughts
---

Dear friends,

Next Saturday, April 29 the Melodica Drone and Bach Quartet will be hosting our first open jam! Come enjoy the beautiful sun with us in Central Park while we all read some music together, jam, and make friends!

[You can find details and RSVP here](https://docs.google.com/forms/d/e/1FAIpQLSeyjxGVrLOVyTmdQvExiLMWgDd2Rd1ldnRETZyUlEcelKhd0A/viewform?usp=sf_link). The event is free, but please RSVP because we will need an email address to tell you the exact location when we find it.

In other news.. [I'm in the news!](https://www.columbiatribune.com/story/entertainment/books/2023/04/17/andrew-yoon-brings-the-poetry-of-taking-chances-to-unbound-fest/70080343007/) Aarik Danielsen from The Columbia Tribune wrote a lovely profile on me ahead of my appearance this weekend at the [Unbound Book Festival](https://www.unboundbookfestival.com/). The piece is very faithful to what I'm trying to do with my [chance poetry](https://weareinvitedtoclimb.org/) and more generally in my work; it's a real honor.

Lastly, I finally finished major edits for my upcoming album, the complete Bach cello suites on melodica. We'll be mixing and mastering it next week, with a release to coincide with the next MDBQ show, probably in early June. Stay "tuned"!

~ay
