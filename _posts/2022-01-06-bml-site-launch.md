---
layout: post
title: Learn how to write text that changes!
date: 2022-01-06
public: true
permalink: /blog/bml-site-launch
category: thoughts
---

Happy 2022, friends!

Over the past couple months I've been hard at work on the chance text programming language behind my recent [book](https://weareinvitedtoclimb.org/) and projects, **BML**, and today I'm thrilled to announce it's ready for others to start using. Some of the highlights of recent work:

* [bml-lang.org](https://bml-lang.org) now contains extensive and approachable documentation for the language, with an eye toward accessibility to non-programmers.
* You can now easily play around with BML online at [bml-lang.org/sandbox](https://bml-lang.org/sandbox/), complete with syntax highlighting.
* I've also released a [VS Code extension](https://marketplace.visualstudio.com/items?itemName=bml-lang.bml-vscode) for the language which lets you easily edit BML documents offline with full syntax highlighting and a built-in runner.
* Many parts of the language have been overhauled to make it more intuitive and powerful, including a system for context-aware choice references. I'm fairly confident I've now worked out all the major breaking changes needed in the foreseeable future.

If you're interested in dipping your toes into the great fun of writing words that change, please let me know! I'm looking for a few people—programmers and non-programmers alike—who are interested in going through the website and trying to learn the language. I would love to hear your feedback on the language and ways I can make it easier to learn, and I would _love_ to see what you write with it!

Until next time, stay warm out there! <3  
~ ay
